if(Notification.permission !== 'granted'){
    Notification.requestPermission().then(res=>{
        if(Notification.permission === 'granted'){
            const hi = new Notification('',{body:'Bienvenido - Welcome - Bienvenu'})
        }
    })
}

const bodyS = document.querySelector('body')
const link = document.querySelector('a')
const day = document.querySelector('#nightB')
const night = document.querySelector('#dayB')
const dataCont = document.querySelector('.student')


function showInfo({nombre, correo, idiomas}) {
    const  id = nombre[0] + parseInt(Math.random()*10)
    const ele = ` <div  id=${id}>
    <h1>Felicitaciones ${nombre}. <br> Pronto nos comunicaremos contigo por medio de tu correo ${correo}.  <br>Cumpliremos tu sueño de aprender el idioma que quieres 
    ${idiomas}.  </h1>
    
</div>
`
    dataCont.insertAdjacentHTML("afterbegin", ele)
     setTimeout(() => {
        const ll = document.querySelector('#'+id)
        ll.remove()
    }, 10000); 
    
}

const modeClick = (e)=>{
    if(e.target.id === 'nightB'){
        if(!bodyS.classList.contains('nightbg')){
            bodyS.classList.add('nightbg')
            link.classList.add('link') 
            alert('Night mode se ha activado!')
        }else{
            alert('Night mode ya esta activado!')
        }
    }
    else{
        if(bodyS.classList.contains('nightbg')){
            bodyS.classList.remove('nightbg')
            link.classList.remove('link') 
            alert('Day mode se ha activado!')

        }else{
            alert('Day mode ya esta activado!')
        }
    }
    
}
day.addEventListener('click',modeClick)
night.addEventListener('click',modeClick)

//form btn
const inpupts = document.getElementsByClassName('inputf')
const registerB = document.getElementById('registerid')
registerB.addEventListener('click',registerClick)

const radios = document.getElementsByClassName('radio')
const checks = document.getElementsByClassName('check')


function getRadio(dictData) {
    var index = 0;
    while(index < radios.length){
        if(radios[index].checked){
            console.log(radios[index].value);
            dictData['nativo'] = radios[index].value
            break
        }
        index++
    }
}
function getCheck(dictData) {
    var indexC = 0;
    var arr= [];
    while(indexC < checks.length){
        if(checks[indexC].checked){
            console.log(checks[indexC].value);
            arr.push(checks[indexC].value)
        }
        indexC++
    }
    dictData['idiomas'] = arr;

}
function getInfo() {
    const dataDict = {}
    for(var i=0;i<inpupts.length;i++){
        console.log( inpupts[i].value,  inpupts[i].id);
        dataDict[inpupts[i].id] = inpupts[i].value;
    }
    getRadio(dataDict)
    getCheck(dataDict)
    console.log(dataDict, dataDict['nombre']);
    showInfo(dataDict)


}

function registerClick(e) {
    e.preventDefault()
    getInfo()

}
